# Tech radar


[![pipeline status](https://gitlab.com/kat3891/tech-radar/badges/master/pipeline.svg)](https://gitlab.com/kat3891/tech-radar/commits/master)
[![coverage report](https://gitlab.com/kat3891/tech-radar/badges/master/coverage.svg)](https://gitlab.com/kat3891/tech-radar/commits/master)

Tech Radar is a tool to help engineering teams to pick the best to pick the best technologies for new projects.
It provides a platform to share knowledge and experience in technologies, 
to reflect on technology decisions and continuously evolve your technology landscape. 

This project provides you a tool to display a nice graph based on a `.tsv` file (that you can generate by downloading a Gsheet for instance).

This work has been made based on [Zalando work](https://github.com/zalando/tech-radar), which is based on [ThoughtWork work](https://www.thoughtworks.com/radar).


## Project structure

- `archive`: contains your Tech Radar files. Each folder inside has a date as a name, for historization.
It currently contains an example.

- `template`: contains the HTML static files used to build the graph.
   - `index.html`: it explains the different rings, calls `d3.js` and `radar.js` script, contains an empty `svg` tag which will contain the graph as scripts are running in your browser.
   - `radar.js`: script to build the graph. It is not complete, since some data coming from the `.tsv` file need to be added.
   - `style.css`: stylesheet

- `build_html.py`: a Python module that contains functions to adapt the template and create HTML and JS static files based on your `tsv` file.

- `run.py`: the Python script to run to generate the HTML and JS static files.

## Example

You can find an example in the `archive` folder. 
It was build the following way:

1. The `data.tsv` file is the example given by [ThoughtWorks](https://www.thoughtworks.com) on 
[Gsheet](https://docs.google.com/spreadsheets/d/1waDG0_W3-yNiAaUfxcZhTKvl7AUCgXwQw8mdPjCz86U/edit#gid=0).
I stored it in `archive/2019-01/` as `data.tsv`.

2. Environment variables were defined. In my case, I used an `.env` file:
```
TSV_PATH: "./archive/2019-01/data.tsv"
TSV_CONF: { "label": 0, "ring": 1, "quadrant": 2, "moved": 3, "description": 4}
DATE: 2019-01
TITLE: Tech Radar
ARCHIVE_PATH: "./archive"
QUADRANTS_CONF: {"tools": 1,"languages & frameworks": 2, "platforms": 3, "techniques": 0}
```

3. Just run the script `run.py`. You can see that a folder called `html` has been created and contains some files.

5. Open `archive/<date>/html/index.html` file in your browser. You can now see your graph.

## Testing

Run tests using:

```bash
pipenv run python setup.py test
```
If the code coverage shall be measured, you can run the code quality using 
`coverage run` instead of `python`. In order to obtain a report 
run `pipenv run coverage html` or `pipenv run coverage report`.

## TODO

- Convert HTML to PDF for archive
