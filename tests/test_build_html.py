import pytest
import src.build_html as m


def test_generate_quadrants():
    assert m.generate_quadrants({
        "tools": 1,
        "languages & frameworks": 2,
        "platforms": 3,
        "techniques": 0,
    }) == [
        {'"name"': '"Techniques"'},
        {'"name"': '"Tools"'},
        {'"name"': '"Languages and frameworks"'},
        {'"name"': '"Platforms"'},
    ]


@pytest.mark.parametrize("row, expected", [
    ("ELK stack\tadopt\ttools\tFALSE\tblablabla",
        {
            "label": '"ELK stack"',
            "quadrant": '"1"',
            "ring": '"0"',
            "moved": '"0"',
        }),
    ("Example\thold\tplatforms\tMOVED UP\tblablabla",
        {
            "label": '"Example"',
            "quadrant": '"3"',
            "ring": '"3"',
            "moved": '"1"',
        }),
    ("ELK stack\tassess\ttechniques\tMOVED DOWN\tblablabla",
        {
            "label": '"ELK stack"',
            "quadrant": '"0"',
            "ring": '"2"',
            "moved": '"-1"',
        }),
    ("ELK stack\tassess\ttechniques\tMOVED UP\tblablabla",
        {
            "label": '"ELK stack"',
            "quadrant": '"0"',
            "ring": '"2"',
            "moved": '"1"',
        }),
    ("ELK stack\tassess\tplatforms\tMOVED DOWN\tblablabla",
        {
            "label": '"ELK stack"',
            "quadrant": '"3"',
            "ring": '"2"',
            "moved": '"-1"',
        }),
    ])
def test_build_js_entry(row, expected):
    tsv_conf = {
        "label": 0,
        "ring": 1,
        "quadrant": 2,
        "moved": 3,
        "description": 4
    }
    quadrants_conf = {
        "tools": 1,
        "languages & frameworks": 2,
        "platforms": 3,
        "techniques": 0,
    }
    row = row.split("\t")
    assert m.build_js_entry(tsv_conf, quadrants_conf, row) == expected


@pytest.mark.parametrize("value, expected", [
    ("ELK stack", '"ELK stack"'),
    ("Rock'n roll", '"Rock\'n roll"'),
])
def test_escape_quotes(value, expected):
    assert m.escape_quotes(value) == expected


def test_generate_js_entries():
    pass


def test_generate_html():
    pass


if __name__ == '__main__':
    pytest.main()
