import os
import json
from src.build_html import generate_html

if __name__ == '__main__':
    kwargs = {
        "date": os.environ["DATE"],
        "title": os.environ["TITLE"],
        "archive_path": os.environ["ARCHIVE_PATH"],
        "quadrants_conf": json.loads(os.environ["QUADRANTS_CONF"]),
        "tsv_conf": {
            "path": os.environ["TSV_PATH"],
            "schema": json.loads(os.environ["TSV_CONF"]),
        }
    }
    generate_html(**kwargs)
