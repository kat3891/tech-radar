radar_visualization({
  "svg_id": "radar",
  "width": 1550,
  "height": 1000,
  "colors": {
    "background": "#fff",
    "grid": "#bbb",
    "inactive": "#ddd"
  },
  "rings": [
    { "name": "ADOPT", "color": "#93c47d" },
    { "name": "TRIAL", "color": "#93d2c2" },
    { "name": "ASSESS", "color": "#fbdb84" },
    { "name": "HOLD", "color": "#efafa9" }
  ],
  "print_layout": true,
  // zoomed_quadrant: 0,
