import os
import logging

from src.conf import TechRadarConf, RINGS_CONF, moved_translate


def generate_quadrants(quadrants):
    res = []
    for k in sorted(quadrants, key=quadrants.get, reverse=False):
        res.append(
            {'"name"': '\"' + k.replace("&", "and").capitalize() + '\"'}
        )
    return res


def escape_quotes(value):
    value = str(value)
    contains = []
    try:
        p = value.index("'")
        value[p] = "\\" + value[p]
        contains.append("'")
    except Exception:
        pass
    try:
        p = value.index('"')
        value[p] = "\\" + value[p]
        contains.append('"')
    except Exception:
        pass
    if len(contains) == 2:
        raise Exception("Label can't contain \' and \" at the same time")
    if '"' in contains:
        return "\'" + value + "\'"
    return '\"' + value + '\"'


def build_js_entry(tsv_conf, quadrants_conf, row):
    """
    Generate information about one technology.
    :param tsv_conf: specify the position of each technology detail in a row
    :type tsv_conf: dict
    :param quadrants_conf: specify the number associated to a quadrant name
    (which is a technology category)
    :type quadrants_conf: dict
    :param row: information about one technology. It is a row of the tsv file
    :type row: list
    :return: a dictionary
    """
    label = row[tsv_conf["label"]].strip()
    quadrant_name = row[tsv_conf["quadrant"]].strip()
    ring_name = row[tsv_conf["ring"]].strip()
    is_new = row[tsv_conf["moved"]].strip()
    m = moved_translate[is_new]
    ring = RINGS_CONF[ring_name]
    q = quadrants_conf[quadrant_name]
    if ring == 0:
        m = abs(moved_translate[is_new])
    return {
        # in case the content of the tsv file contains some quotes
        # we "escape" it, so it will not be considered as the end of a
        # string by python or by bash
        "label": escape_quotes(label),
        "quadrant": escape_quotes(q),
        "ring": escape_quotes(ring),
        "moved": escape_quotes(m),
    }


def generate_js_entries(tsv_conf, quadrants_conf, data_path):
    """ Get data from the tsv file and generate the proper javascript data.
     It returns a list of dictionaries.
    :param tsv_conf: specify the position of each technology detail in a row
    :type tsv_conf: dict
    :param quadrants_conf: specify the number associated to a quadrant name
    (which is a technology category)
    :type quadrants_conf: dict
    :param data_path: the location of the tsv file
    :type data_path: str
    :return: a list of dictionaries
    """
    # read the file
    with open(data_path, "r") as f:
        file = f.read()
        # split the file by row
        if "\r\n" in file:
            file = file.split("\r\n")
        else:
            file = file.split("\n")

        # for each row (without the first one, which contains column names),
        # build the proper js entry
        logging.warning("Generating graph data...")
        entries = [
            build_js_entry(tsv_conf, quadrants_conf, row.split("\t"))
            for row in file[1:]
        ]

    logging.warning("Graph data generated. Data: {}".format(entries))

    return entries


def generate_html(**kwargs):
    """Generate the HTML and Javascript files that will display
    the tech radar graph"""
    logging.warning("Starting to generate HTML and Javascript files...")
    # define conf
    conf = TechRadarConf(**kwargs)
    # TODO check file existence before

    # create the HTML repository that will containe the HTML and JS files
    logging.warning("Create the repository that will contains HTML "
                    "and Javascript files... {}")
    os.system("mkdir -p {}".format(conf.html.folder[:-1]))
    logging.warning("Repository {} created".format(conf.html.folder[:-1]))

    # copy the template repository
    logging.warning("Adding template content...")
    os.system("cp ./src/template/* {}".format(conf.html.folder))

    # generate the required data for the javascript file `generate_graph.js`
    # - the graph data in the proper format
    # - the quadrants conf (the proper names to display and the sorting)
    logging.warning("Generating data for the javascript script "
                    "generate_graph.js...")
    entries = generate_js_entries(
        conf.tsv.schema,
        conf.quadrants,
        conf.tsv.path)
    q = generate_quadrants(conf.quadrants)

    # define the title of the graph in the javascript file `generate_graph.js`
    logging.warning("Adding graph title in the javascript script...")
    os.system('echo \\"title\\": \\"'
              + conf.title + '\\", >> ' + conf.html.js_file)

    # specify the quadrants conf  in the javascript file `generate_graph.js`
    logging.warning("Adding technologies categories names in the "
                    "javascript script...")
    os.system('echo \\"quadrants\\": [ >> ' + conf.html.js_file)
    for e in q:
        os.system("echo " + str(e) + ", >> " + conf.html.js_file)
    os.system("echo ], >> " + conf.html.js_file)

    # specify the graph data in the javascript file `generate_graph.js`
    logging.warning("Adding graph data in the javascript script...")
    os.system('echo \\"entries\\": [ >> ' + conf.html.js_file)
    for e in entries:
        os.system("echo {}, >> {}".format(e, conf.html.js_file))
    # close the function that will use the previous provided data
    os.system("echo ']});' >> " + conf.html.js_file)

    logging.warning("HTML and JS files generated.")
