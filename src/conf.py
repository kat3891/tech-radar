# Define the position of each ring in the graph (should not change)
RINGS_CONF = {
    "trial": 1,
    "adopt": 0,
    "assess": 2,
    "hold": 3
}


class HtmlRadarConf(object):
    def __init__(self, repo):
        self.folder = repo + "html/"
        self.index_path = self.folder + "index.html"
        self.js_file = self.folder + "generate_graph.js"


class TsvRadarConf(object):
    def __init__(self, tsv_conf):
        self.path = tsv_conf["path"]
        self.schema = tsv_conf["schema"]


class TechRadarConf(object):
    def __init__(self, date, title, archive_path, quadrants_conf, tsv_conf):
        self.date = date
        self.title = title + " — " + self.date
        self.folder_path = archive_path + "/" + self.date + "/"

        self.html = HtmlRadarConf(self.folder_path)
        self.tsv = TsvRadarConf(tsv_conf)
        self.quadrants = quadrants_conf


# Dictionary that translate the content of the column
# "Has the technology changed of ring ?"
# in the proper number to draw the graph
moved_translate = {
    "no": 0,
    "": 0,
    "in": -1,
    "out": 1,
    "MOVED UP": 1,
    "Moved Up": 1,
    "Moved up": 1,
    "moved up": 1,
    "MOVED DOWN": -1,
    "Moved Down": -1,
    "Moved down": -1,
    "moved down": -1,
    "FALSE": 0,
    "False": 0,
    "false": 0,
    "TRUE": 1,
    "True": 1,
    "true": 1,
}
