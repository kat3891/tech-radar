from setuptools import setup, find_packages

setup(
    name="tech-radar",
    version="0.0.1",
    packages=find_packages("src"),
    package_dir={"": "src"},
    setup_requires=["pytest-runner"],
    tests_require=["pytest"]
)
